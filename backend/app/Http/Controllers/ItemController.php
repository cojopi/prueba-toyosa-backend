<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Item;
use App\Http\Controllers\UserController;

class ItemController extends Controller
{
    public function index()
    {
        $items = Item::all();
        return $items;
    }

    public function store(Request $request)
    {
        $idUser = (new UserController)->isLoggedIn($request);
        if ($idUser) {
            $item = new Item();
            $item->name = $request->name;
            $item->text = $request->text;
            $item->idUser = $idUser;
            $item->save();
        }
    }

    public function getUserItems(Request $request)
    {
        $idUser = (new UserController)->isLoggedIn($request);
        if ($idUser) {
            $items = Item::where('idUser', $idUser)->get();
            return $items;
        }
    }
}
