<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function index()
    {
        $users = User::all();
        return $users;
    }

    public function register(Request $request)
    {
        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->save();
    }

    public function login(Request $request)
    {
        $user = User::where('username', $request->username)->where('password', $request->password)->first();
        if ($user) {
            $user->token = str_random(16);
            $user->save();
            return $user->token;
        }
        else {
            return "";
        }
    }

    public function isLoggedIn(Request $request)
    {
        $user = User::where('token', $request->token)->first();
        if ($user) {
            return $user->id;
        }
        else {
            return "";
        }
    }
}
